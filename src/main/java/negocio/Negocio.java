/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import com.toedter.calendar.JDateChooser;
import datos.datos;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fabri
 */
public class Negocio {

    datos oDatos = new datos();

    public String[] CMBaerolineas() {

        String[][] datos = oDatos.leerAerolineas();
        String[] aerolineas = new String[datos.length];
        for (int i = 0; i < datos.length; i++) {
            aerolineas[i] = datos[i][1];
        }
        return aerolineas;
    }

    public String[] CMBAeropuertos() {

        String[][] datos = oDatos.leerAeropuertos();
        String[] aeropuertos = new String[datos.length];
        for (int i = 0; i < datos.length; i++) {
            aeropuertos[i] = datos[i][0] + " : " + datos[i][2];
        }
        return aeropuertos;
    }

    public String login(String usuario, String contraseña) {
        String tipo = "";
        String[][] usuarios = oDatos.leerUsuarios();
        for (int i = 0; i < usuarios.length; i++) {
            if (usuario.equals(usuarios[i][0]) && contraseña.equals(usuarios[i][3])) {
                tipo = usuarios[i][4];
                break;
            } else {
                tipo = "Usuario o contraseña incorrecto";
            }
        }
        return tipo;
    }

    public String asignarPilotos(String aerolinea) {
        int contador = 0;
        String pilotos = "";
        String[][] usuarios = oDatos.leerUsuarios();

        for (int i = 0; i < usuarios.length; i++) {
            if (usuarios[i][4].equalsIgnoreCase("tripulante")) {
                if (usuarios[i][6].equalsIgnoreCase(aerolinea)) {
                    if (usuarios[i][7].equalsIgnoreCase("piloto")) {
                        if (usuarios[i][8].equalsIgnoreCase("disponible")) {
                            pilotos += usuarios[i][0];
                            contador++;
                            if (contador == 2) {
                                break;
                            } else {
                                pilotos += "-";
                            }
                        }
                    }
                }
            }
        }
        if (contador < 2) {
            pilotos = "No hay suficientes pilotos disponibles";
        }
        return pilotos;
    }
    

    public String asignarCS(String aerolinea) {
        int contador = 0;
        String servicio = "";
        String[][] usuarios = oDatos.leerUsuarios();

        for (int i = 0; i < usuarios.length; i++) {
            if (usuarios[i][4].equalsIgnoreCase("tripulante")) {
                if (usuarios[i][6].equalsIgnoreCase(aerolinea)) {
                    if (usuarios[i][7].equalsIgnoreCase("servicio al cliente")) {
                        if (usuarios[i][8].equalsIgnoreCase("disponible")) {
                            servicio += usuarios[i][0];
                            contador++;
                            if (contador == 3) {
                                break;
                            } else {
                                servicio += "-";
                            }
                        }
                    }
                }
            }
        }
        if (contador < 3) {
            servicio = "No hay suficientes tripulantes disponibles";
        }
        return servicio;
    }
    
    
    
    
    
    
    
    public DefaultTableModel BuscarTripulantes(String aerolinea, DefaultTableModel pModel) {
        DefaultTableModel modelo = pModel ;
        String[][] usuarios = oDatos.leerUsuarios();

        for (int i = 0; i < usuarios.length; i++) {
            if (usuarios[i][4].equalsIgnoreCase("tripulante")) {
                if (usuarios[i][6].equalsIgnoreCase(aerolinea)) {
                    modelo.addRow(new Object[]{usuarios[i][0],usuarios[i][1],usuarios[i][5],usuarios[i][7],usuarios[i][8]});
                }
            }
        }
        
        
        return modelo;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public String asignarAvion(String aerolinea) {
        String avion = "";
        String[][] aviones = oDatos.leerAviones();

        for (int i = 0; i < aviones.length; i++) {
            if (aviones[i][3].equalsIgnoreCase(aerolinea)) {
                if (aviones[i][5].equalsIgnoreCase("disponible")) {
                    avion = aviones[i][0];
                }
            }
        }
        return avion;
    }



  

    public DefaultTableModel buscarVuelos(JDateChooser pSalida, JDateChooser pLlegada, String pAsalida, String pAllegada) {
        
        long horas = 0;
        String[][] vuelos = oDatos.leerVuelos();
        String[] nombreColumnas = {"IDA",
            "Precio vuelo", "Duración", "Fecha y hora salida",
            "Aeropuerto salida", "Fecha y hora llegada", "Aeropuerto llegada", "Escala"};
         DefaultTableModel modelo = new DefaultTableModel(nombreColumnas, 0);
        for (int i = 0; i < vuelos.length; i++) {

            String[] fechaSalida = sacarFecha(vuelos[i][3]);
            String[] fechaLlegada = sacarFecha(vuelos[i][5]);
            if ((fechaSalida[0].equals(fechaSalida(pSalida))) && (fechaLlegada[0].equals(fechaLlegada(pLlegada))) && (pAsalida.equals(vuelos[i][4])) && (pAllegada.equals(vuelos[i][6]))) {
                horas = sacarDuracion(vuelos[i][3], vuelos[i][5]);
               // Object fila[] = new Object[]{vuelos[i][0], vuelos[i][1], (int) horas , vuelos[i][3], vuelos[i][4], vuelos[i][5], vuelos[i][6], "NO"};
                modelo.addRow(new Object[]{vuelos[i][0], vuelos[i][1], (int) horas , vuelos[i] [3], vuelos[i][4], vuelos[i][5], vuelos[i][6], "NO"});
        

            } else {
                if ((fechaSalida[0].equals(fechaSalida(pSalida))) && (pAsalida.equals(vuelos[i][4]))) {
                    String salidaNueva = vuelos[i][6];
                    int precio = Integer.parseInt(vuelos[i][1]);
                    System.err.println(precio);
                    for (int x = 0; x < vuelos.length; x++) {
                        String[] fechaLlegada2 = sacarFecha(vuelos[x][5]);
                        long validaHoraSalida = sacarDuracion(vuelos[i][5], vuelos[x][3]);
                        System.out.println(validaHoraSalida);
                        if ((salidaNueva.equals(vuelos[x][4]) && (validaHoraSalida > 0) && (fechaLlegada2[0].equals(fechaLlegada(pLlegada))) && (pAllegada.equals(vuelos[x][6])))) {
                            horas = sacarDuracion(vuelos[i][3], vuelos[x][5]);
                            int sumar = Integer.parseInt(vuelos[x][1]);
                            System.err.println(sumar);
                            sumar = sumar + precio;
                            String fila[] = {vuelos[i][0] + "-" + vuelos[x][0], "$" + sumar, horas + " horas", vuelos[i][3], vuelos[i][4], vuelos[x][5], vuelos[x][6], "SI 1 en " + vuelos[x][4]};
                            modelo.addRow(fila);
                        } else {
                            if ((salidaNueva.equals(vuelos[x][4]) && (validaHoraSalida > 0))) {
                                String salidaNueva3 = vuelos[x][6];
                                int precio3 = Integer.parseInt(vuelos[x][1]);
                                for (int y = 0; y < vuelos.length; y++) {
                                    String[] fechaLlegada3 = sacarFecha(vuelos[y][5]);
                                    long validaHoraSalida3 = sacarDuracion(vuelos[x][5], vuelos[y][3]);
                                    System.out.println(validaHoraSalida);
                                    if ((salidaNueva3.equals(vuelos[y][4]) && (validaHoraSalida3 > 0) && (fechaLlegada3[0].equals(fechaLlegada(pLlegada))) && (pAllegada.equals(vuelos[y][6])))) {
                                        horas = sacarDuracion(vuelos[i][3], vuelos[y][5]);
                                        int sumar = Integer.parseInt(vuelos[y][1]);
                                        System.err.println(sumar);
                                        sumar = sumar + precio+precio3;
                                        String fila[] = {vuelos[i][0] + "-" + vuelos[x][0]+"-"+vuelos[y][0], "$" + sumar, horas + " horas", vuelos[i][3], vuelos[i][4], vuelos[y][5], vuelos[y][6], "SI 2 en " + vuelos[x][4]+" y "+vuelos[y][4]};
                                        modelo.addRow(fila);

                                    }
                                }
                            }

                        }

                    }
                }

            }

        }

        return modelo;
    }
    
    
    
    public DefaultTableModel rankingAviones(DefaultTableModel pModel){
        String [][] datosVuelos =oDatos.leerVuelos();
        String [][] datosAviones = oDatos.leerAviones();
        DefaultTableModel modelo= pModel;
         for (int i = 0; i < datosVuelos.length; i++) {
             
             for (int j = 0; j < datosAviones.length; j++) {
                 if (datosVuelos[i][9].equals(datosAviones[j][0])){
                     modelo.addRow(new Object [] {datosAviones[j][0],datosAviones[j][1],datosAviones[j][2],datosAviones[j][3],datosAviones[j][4],datosAviones[j][5]}  );
                     
                 }
             }
        }
        
        return modelo;  
    }
    
    
    
    
    
    
    
    
     public DefaultTableModel buscarVuelosInteligentes (Date pSalida, Date pLlegada, String pAsalida, String pAllegada, DefaultTableModel pModelo) {
        
        long horas = 0;
        String[][] vuelos = oDatos.leerVuelos();
        String[] nombreColumnas = {"IDA",
            "Precio vuelo", "Duración", "Fecha y hora salida",
            "Aeropuerto salida", "Fecha y hora llegada", "Aeropuerto llegada", "Escala"};
         DefaultTableModel modelo = pModelo;
        for (int i = 0; i < vuelos.length; i++) {

            String[] fechaSalida = sacarFecha(vuelos[i][3]);
            String[] fechaLlegada = sacarFecha(vuelos[i][5]);
            if ((fechaSalida[0].equals(fechaSalidaI(pSalida))) && (fechaLlegada[0].equals(fechaLlegadaI(pLlegada))) && (pAsalida.equals(vuelos[i][4])) && (pAllegada.equals(vuelos[i][6]))) {
                horas = sacarDuracion(vuelos[i][3], vuelos[i][5]);
               // Object fila[] = new Object[]{vuelos[i][0], vuelos[i][1], (int) horas , vuelos[i][3], vuelos[i][4], vuelos[i][5], vuelos[i][6], "NO"};
                modelo.addRow(new Object[]{vuelos[i][0], vuelos[i][1], (int) horas , vuelos[i] [3], vuelos[i][4], vuelos[i][5], vuelos[i][6], "NO"});
        

            } else {
                if ((fechaSalida[0].equals(fechaSalidaI(pSalida))) && (pAsalida.equals(vuelos[i][4]))) {
                    String salidaNueva = vuelos[i][6];
                    int precio = Integer.parseInt(vuelos[i][1]);
                   // System.err.println(precio);
                    for (int x = 0; x < vuelos.length; x++) {
                        String[] fechaLlegada2 = sacarFecha(vuelos[x][5]);
                        long validaHoraSalida = sacarDuracion(vuelos[i][5], vuelos[x][3]);
                       // System.out.println(validaHoraSalida);
                        if ((salidaNueva.equals(vuelos[x][4]) && (validaHoraSalida > 0) && (fechaLlegada2[0].equals(fechaLlegadaI(pLlegada))) && (pAllegada.equals(vuelos[x][6])))) {
                            horas = sacarDuracion(vuelos[i][3], vuelos[x][5]);
                            int sumar = Integer.parseInt(vuelos[x][1]);
                           // System.err.println(sumar);
                            sumar = sumar + precio;
                            String fila[] = {vuelos[i][0] + "-" + vuelos[x][0], "$" + sumar, horas + " horas", vuelos[i][3], vuelos[i][4], vuelos[x][5], vuelos[x][6], "SI 1 en " + vuelos[x][4]};
                            modelo.addRow(fila);
                        } else {
                            if ((salidaNueva.equals(vuelos[x][4]) && (validaHoraSalida > 0))) {
                                String salidaNueva3 = vuelos[x][6];
                                int precio3 = Integer.parseInt(vuelos[x][1]);
                                for (int y = 0; y < vuelos.length; y++) {
                                    String[] fechaLlegada3 = sacarFecha(vuelos[y][5]);
                                    long validaHoraSalida3 = sacarDuracion(vuelos[x][5], vuelos[y][3]);
                                  //  System.out.println(validaHoraSalida);
                                    if ((salidaNueva3.equals(vuelos[y][4]) && (validaHoraSalida3 > 0) && (fechaLlegada3[0].equals(fechaLlegadaI(pLlegada))) && (pAllegada.equals(vuelos[y][6])))) {
                                        horas = sacarDuracion(vuelos[i][3], vuelos[y][5]);
                                        int sumar = Integer.parseInt(vuelos[y][1]);
                                    //    System.err.println(sumar);
                                        sumar = sumar + precio+precio3;
                                        String fila[] = {vuelos[i][0] + "-" + vuelos[x][0]+"-"+vuelos[y][0], "$" + sumar, horas + " horas", vuelos[i][3], vuelos[i][4], vuelos[y][5], vuelos[y][6], "SI 2 en " + vuelos[x][4]+" y "+vuelos[y][4]};
                                        modelo.addRow(fila);

                                    }
                                }
                            }

                        }

                    }
                }

            }

        }

        return modelo;
    }
    
    
    private String[] sacarFecha(String pFecha) {
        String[] retorno = pFecha.split("-");
        return retorno;
    }

    private long sacarDuracion(String pFechaSalida, String pFechaLlegada) {
        long horas = 0;
        try {

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-hh:mm");
            Date salida = dateFormat.parse(pFechaSalida);
            Date llegada = dateFormat.parse(pFechaLlegada);
            long miliseg = llegada.getTime() - salida.getTime();
            horas = TimeUnit.HOURS.convert(miliseg, TimeUnit.MILLISECONDS);

        } catch (ParseException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return horas;
    }

    private String fechaSalida(JDateChooser dateSalida) {
        Date fecha = dateSalida.getDate();
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaConvertida = formato.format(fecha);
        return fechaConvertida;
    }

    private String fechaLlegada(JDateChooser dateLlegada) {
        Date fecha = dateLlegada.getDate();
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaConvertida = formato.format(fecha);
        return fechaConvertida;
    }
    
      private String fechaSalidaI(Date dateLlegada) {
      
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaConvertida = formato.format(dateLlegada);
        return fechaConvertida;
    }

    private String fechaLlegadaI(Date dateLlegada) {
       
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String fechaConvertida = formato.format(dateLlegada);
        return fechaConvertida;
    }
    
    
    
    
}
