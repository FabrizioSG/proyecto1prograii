/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import com.utn.utilitarios.archivoSecuencial;

/**
 *
 * @author fabri
 */
public class datos {
    
    public void agregar(String linea, String documento){
        archivoSecuencial oArch = new archivoSecuencial("src\\main\\java\\archivos",documento);
        String vuelo = linea;
        oArch.escribir(oArch.leer() + vuelo);
    }
    
    public String[][] leerAerolineas(){
        archivoSecuencial oArch = new archivoSecuencial("src\\main\\java\\archivos","Aerolineas.txt");
        String[] linea = oArch.leerArray();
        String[][] matriz = new String[linea.length][4];
        for(int i=0; i<linea.length; i++){
            String[] separado = linea[i].split(",");
            
            matriz[i][0] = separado[0];
            matriz[i][1] = separado[1];
            matriz[i][2] = separado[2];
            matriz[i][3] = separado[3];
            
        }
        
        return matriz;
    }
    public String[][] leerAeropuertos(){
        archivoSecuencial oArch = new archivoSecuencial("src\\main\\java\\archivos","Aeropuertos.txt");
        String[] linea = oArch.leerArray();
        String[][] matriz = new String[linea.length][4];
        for(int i=0; i<linea.length; i++){
            String[] separado = linea[i].split(",");
            
            matriz[i][0] = separado[0];
            matriz[i][1] = separado[1];
            matriz[i][2] = separado[2];
            matriz[i][3] = separado[3];
            
        }
        
        return matriz;
    }
    public String[][] leerAviones(){
        archivoSecuencial oArch = new archivoSecuencial("src\\main\\java\\archivos","Aviones.txt");
        String[] linea = oArch.leerArray();
        String[][] matriz = new String[linea.length][6];
        for(int i=0; i<linea.length; i++){
            String[] separado = linea[i].split(",");
            
            matriz[i][0] = separado[0];
            matriz[i][1] = separado[1];
            matriz[i][2] = separado[2];
            matriz[i][3] = separado[3];
            matriz[i][4] = separado[4];
            matriz[i][5] = separado[5];
            
        }
        
        return matriz;
    }
    public String[][] leerVuelos(){
        archivoSecuencial oArch = new archivoSecuencial("src\\main\\java\\archivos","Vuelos.txt");
        String[] linea = oArch.leerArray();
        String[][] matriz = new String[linea.length][10];
        for(int i=0; i<linea.length; i++){
            String[] separado = linea[i].split(",");
            
            matriz[i][0] = separado[0];
            matriz[i][1] = separado[1];
            matriz[i][2] = separado[2];
            matriz[i][3] = separado[3];
            matriz[i][4] = separado[4];
            matriz[i][5] = separado[5];
            matriz[i][6] = separado[6];
            matriz[i][7] = separado[7];
            matriz[i][8] = separado[8];
            matriz[i][9] = separado[9];
                        
        }
        
        return matriz;
    }
    public String[][] leerUsuarios(){
        archivoSecuencial oArch = new archivoSecuencial("src\\main\\java\\archivos","Usuarios.txt");
        String[] linea = oArch.leerArray();
        String[][] matriz = new String[linea.length][9];
        for(int i=0; i<linea.length; i++){
            String[] separado = linea[i].split(",");
            
            matriz[i][0] = separado[0];
            matriz[i][1] = separado[1];
            matriz[i][2] = separado[2];
            matriz[i][3] = separado[3];
            matriz[i][4] = separado[4];
            matriz[i][5] = separado[5];
            matriz[i][6] = separado[6];
            matriz[i][7] = separado[7];
            matriz[i][8] = separado[8];
                                   
        }
        
        return matriz;
    }
    
    
}
